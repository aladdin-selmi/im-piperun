import {Command, Flags} from '@oclif/core'
import fetch from 'node-fetch'

export default class Trigger extends Command {
  static description = 'Trigger pipeline command'

  static examples = [
    '<%= config.bin %> <%= command.id %> mmp-rest dev -t 147d57ca1124cf67b1119fa24dc74b',
  ]

  static projectsIds = {
    'sancella-rest': '30384553',
    'sancella-admin': '30384570',
    'sancella-store': '30384591',
    '3hearts-rest': '26117150',
    '3hearts-admin': '26117191',
    '3hearts-store': '25913152',
    'wallyscar-rest': '27294900',
    'wallyscar-admin': '27294926',
    'wallyscar-store': '27295015',
    'mmp-rest': '34903098',
    'mmp-admin': '30384570',
    'mmp-store': '30384591',
  }

  static projectsTokens = {
    'sancella-rest': 'a0c509dfa05513faefda63492fbbc6',
    // 'sancella-admin': '',
    // 'sancella-store': '',
    '3hearts-rest': '78b4ee9cf17dfd1efde4e1a4154510',
    // '3hearts-admin': '',
    // '3hearts-store': '',
    // 'wallyscar-rest': '',
    // 'wallyscar-admin': '',
    // 'wallyscar-store': '',
    'mmp-rest': '147d57ca1124cf67b1119fa24dc74b',
    // 'mmp-admin': '',
    // 'mmp-store': '',
  }

  static flags = {
    // flag with a value (-n, --name=VALUE)
    token: Flags.string({char: 't', description: 'user token'}),
  }

  static args = [
    {name: 'project', required: true, options: Object.keys(Trigger.projectsIds)},
    {name: 'branch', required: true},
  ]

  public async run(): Promise<void> {
    const {args, flags} = await this.parse(Trigger)

    // AdvancedTechSofts Tokens :
    // mmp-rest : 147d57ca1124cf67b1119fa24dc74b
    // 3c-rest : 78b4ee9cf17dfd1efde4e1a4154510
    // sancella-rest : a0c509dfa05513faefda63492fbbc6

    const token = flags.token || Trigger.projectsTokens[args.project]
    const projectId = Trigger.projectsIds[args.project]
    const branch = args.branch

    const urlGitlab = `https://gitlab.com/api/v4/projects/${projectId}/ref/${branch}/trigger/pipeline?token=${token}`

    this.log('IM-PIPERUN')
    this.log(`Triggering pipeline for project ${projectId} on branch ${branch}`)
    this.log(urlGitlab)

    fetch(urlGitlab, {method: 'POST'})
    .then(res => res.json())
    .then(json => console.log(json))
  }
}
