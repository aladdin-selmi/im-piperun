oclif-hello-world
=================

oclif example Hello World CLI

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/oclif-hello-world.svg)](https://npmjs.org/package/oclif-hello-world)
[![CircleCI](https://circleci.com/gh/oclif/hello-world/tree/main.svg?style=shield)](https://circleci.com/gh/oclif/hello-world/tree/main)
[![Downloads/week](https://img.shields.io/npm/dw/oclif-hello-world.svg)](https://npmjs.org/package/oclif-hello-world)
[![License](https://img.shields.io/npm/l/oclif-hello-world.svg)](https://github.com/oclif/hello-world/blob/main/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
<!-- Install -->
# Install
clone this repo and then
```sh-session
$ npm install
$ npm link
$ npm install -g im-piperun # ignore for now
```
<!-- Installtop -->
# Usage
<!-- usage -->
```sh-session
$ im-piperun COMMAND
running command...
$ im-piperun (--version)
im-piperun/0.0.0 linux-x64 node-v14.17.4
$ im-piperun --help [COMMAND]
USAGE
  $ im-piperun COMMAND
...
```
<!-- usagestop -->
# Primary command
<!-- commands -->
* [`im-piperun trigger [PROJECT] [BRANCH] [-t <value>]`](#im-piperun-trigger)

# Other commands
<!-- commands -->
* [`im-piperun trigger [PROJECT] [BRANCH] [-t <value>]`](#im-piperun-trigger)
* [`im-piperun hello PERSON`](#im-piperun-hello-person)
* [`im-piperun hello world`](#im-piperun-hello-world)
* [`im-piperun help [COMMAND]`](#im-piperun-help-command)
* [`im-piperun plugins`](#im-piperun-plugins)
* [`im-piperun plugins:inspect PLUGIN...`](#im-piperun-pluginsinspect-plugin)
* [`im-piperun plugins:install PLUGIN...`](#im-piperun-pluginsinstall-plugin)
* [`im-piperun plugins:link PLUGIN`](#im-piperun-pluginslink-plugin)
* [`im-piperun plugins:uninstall PLUGIN...`](#im-piperun-pluginsuninstall-plugin)
* [`im-piperun plugins update`](#im-piperun-plugins-update)

## `im-piperun trigger`

Trigger pipeline command

```
USAGE
  $ im-piperun trigger [PROJECT] [BRANCH] [-t <value>]

FLAGS
  -t, --token=<value>  [default: 03302607662264fcdbc7d22d08d611] user token

DESCRIPTION
  Trigger pipeline command

EXAMPLES
  $ im-piperun trigger sancella-rest test
```

## `im-piperun hello PERSON`

Say hello

```
USAGE
  $ im-piperun hello [PERSON] -f <value>

ARGUMENTS
  PERSON  Person to say hello to

FLAGS
  -f, --from=<value>  (required) Whom is saying hello

DESCRIPTION
  Say hello

EXAMPLES
  $ oex hello friend --from oclif
  hello friend from oclif! (./src/commands/hello/index.ts)
```

_See code: [dist/commands/hello/index.ts](https://github.com/aladdin-selmi/im-piperun/blob/v0.0.0/dist/commands/hello/index.ts)_

## `im-piperun hello world`

Say hello world

```
USAGE
  $ im-piperun hello world

DESCRIPTION
  Say hello world

EXAMPLES
  $ oex hello world
  hello world! (./src/commands/hello/world.ts)
```

## `im-piperun help [COMMAND]`

Display help for im-piperun.

```
USAGE
  $ im-piperun help [COMMAND] [-n]

ARGUMENTS
  COMMAND  Command to show help for.

FLAGS
  -n, --nested-commands  Include all nested commands in the output.

DESCRIPTION
  Display help for im-piperun.
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v5.1.11/src/commands/help.ts)_

## `im-piperun plugins`

List installed plugins.

```
USAGE
  $ im-piperun plugins [--core]

FLAGS
  --core  Show core plugins.

DESCRIPTION
  List installed plugins.

EXAMPLES
  $ im-piperun plugins
```

_See code: [@oclif/plugin-plugins](https://github.com/oclif/plugin-plugins/blob/v2.0.11/src/commands/plugins/index.ts)_

## `im-piperun plugins:inspect PLUGIN...`

Displays installation properties of a plugin.

```
USAGE
  $ im-piperun plugins:inspect PLUGIN...

ARGUMENTS
  PLUGIN  [default: .] Plugin to inspect.

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Displays installation properties of a plugin.

EXAMPLES
  $ im-piperun plugins:inspect myplugin
```

## `im-piperun plugins:install PLUGIN...`

Installs a plugin into the CLI.

```
USAGE
  $ im-piperun plugins:install PLUGIN...

ARGUMENTS
  PLUGIN  Plugin to install.

FLAGS
  -f, --force    Run yarn install with force flag.
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Installs a plugin into the CLI.

  Can be installed from npm or a git url.

  Installation of a user-installed plugin will override a core plugin.

  e.g. If you have a core plugin that has a 'hello' command, installing a user-installed plugin with a 'hello' command
  will override the core plugin implementation. This is useful if a user needs to update core plugin functionality in
  the CLI without the need to patch and update the whole CLI.

ALIASES
  $ im-piperun plugins add

EXAMPLES
  $ im-piperun plugins:install myplugin

  $ im-piperun plugins:install https://github.com/someuser/someplugin

  $ im-piperun plugins:install someuser/someplugin
```

## `im-piperun plugins:link PLUGIN`

Links a plugin into the CLI for development.

```
USAGE
  $ im-piperun plugins:link PLUGIN

ARGUMENTS
  PATH  [default: .] path to plugin

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Links a plugin into the CLI for development.

  Installation of a linked plugin will override a user-installed or core plugin.

  e.g. If you have a user-installed or core plugin that has a 'hello' command, installing a linked plugin with a 'hello'
  command will override the user-installed or core plugin implementation. This is useful for development work.

EXAMPLES
  $ im-piperun plugins:link myplugin
```

## `im-piperun plugins:uninstall PLUGIN...`

Removes a plugin from the CLI.

```
USAGE
  $ im-piperun plugins:uninstall PLUGIN...

ARGUMENTS
  PLUGIN  plugin to uninstall

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Removes a plugin from the CLI.

ALIASES
  $ im-piperun plugins unlink
  $ im-piperun plugins remove
```

## `im-piperun plugins update`

Update installed plugins.

```
USAGE
  $ im-piperun plugins update [-h] [-v]

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Update installed plugins.
```
<!-- commandsstop -->
